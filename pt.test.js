// require Playwright using Chrome (chromium)
const { chromium, selectors } = require('playwright'); // https://playwright.dev/docs/intro
var assert = require('assert'); // https://www.w3schools.com/nodejs/ref_assert.asp

(async () => {
  
  // launch browser headless false to see the browser
  const browser = await chromium.launch(
    { 
      headless: false
    });
  const page = await browser.newPage(); 

  // navigate to automationpractice.com
  // https://playwright.dev/docs/api/class-page#page-goto
  await page.goto('http://automationpractice.com'); 
  
  // type "printed summer dress" in the search bar
  // https://playwright.dev/docs/api/class-page#page-fill
  await page.fill('//*[@id="search_query_top"]', "printed summer dress");

  // click the search button
  // https://playwright.dev/docs/api/class-page#page-click
  await page.click('//*[@id="searchbox"]/button');

  // wait for the result page to load
  // https://playwright.dev/docs/api/class-page#page-wait-for-timeout
  await page.waitForTimeout(4000);

  // Mouse hover over the cheapest dress container for "More" button to appear
  // https://playwright.dev/docs/api/class-page#page-hover
  await page.hover('//*[@id="center_column"]/ul/li[3]/div');

  // click the "More" button
  await page.click('//*[@id="center_column"]/ul/li[3]/div/div[2]/div[2]/a[2]/span');

  // wait for the product page to load
  await page.waitForTimeout(4000);

  // click the + button
  await page.click('//*[@id="quantity_wanted_p"]/a[2]');

  // change size M on dropdown
  // https://playwright.dev/docs/input#select-options
  await page.selectOption('//*[@id="group_1"]','2');

  // select green color
  await page.click('//*[@id="color_15"]');

  // click on "Add to Cart" button
  await page.click('//*[@id="add_to_cart"]/button');

  // click on "Proceed to Checkout" button
  await page.click('//*[@id="layer_cart"]/div[1]/div[2]/div[4]/a');

  // get the total price
  // https://playwright.dev/docs/api/class-page#page-inner-text
  var totalprice = await page.innerText('//*[@id="total_price"]');

  // take screenshot
  await page.screenshot({ path: `technicaltest.png` });

  // make the assertion (use == for comparison)
  // https://www.w3schools.com/nodejs/ref_assert.asp
  assert(totalprice == '$34.80');

  // close the browser
  await browser.close();
})();

