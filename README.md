# Technical Test for PT

I used [Playwright](https://playwright.dev/) to solve the exercise. While there's room for improvement, setting it up and getting a working test was less difficult than everything I tried before.

To run the test:

Clone the repo

```
git clone https://gitlab.com/Luthychan7/pt-technical-test
```

Install dependencies (requires [Node](https://nodejs.org/en/)):

```
npm install
```

Run the test:

```
npm test
```

The test takes a screenshot just before doing the assertion, here's an example:

![screenshot](technicaltest.png)

## Setup

Create a node project:

```
npm init
```

Follow the prompts, and change the test command to:

```
node pt.test.js
```

Install [Playwright](https://playwright.dev/docs/intro#installation):

```
npm i -D playwright
```

Create the test file specified in the `run` command (`pt.test.js`). I commented the code as I wrote it to document how I solved every step, including links to the Playwright docs.

To run the test, use `npm test`.

**Note:** I had trouble with Webkit, so I'm using Chromium for the exercise.

